import miepython as mie
import numpy as np
from aerosol_optprop.monodisperse import Monodisperse
from aerosol_optprop.util import CrossSection


class Lognormal(Monodisperse):

    def __init__(self, median_radius: float, width: float, n: float=1.0):
        """
        Class to handle the optical property calculations of a lognormal distribution of spherical sulphate particles.
        Radii and wavelength parameters are in units of microns.

        The equation describing the distribution is:

        .. math::
            dn/dr = n/(\sqrt{2\pi}r\sigma_g)\exp(-(\ln(r)-\ln(r_g))^2/2\sigma_g^2)

        where :math:`r_g` is median radius, and :math:`\sigma_g` is the width. The code uses the
        `miepython <https://github.com/scottprahl/miepython>`_ implementation of the Wiscombe code developed by
        Scott Prahl for Mie calculations.

        Parameters
        ----------
        median_radius :
            median radius of the distribution in microns
        width :
            width parameter of the distribution
        n :
            aerosol number density, default is 1.

            ..math::
                \int_0^\inf pdf(r)dr = n

        Example
        -------
        Calculate the extinction of a lognormal distribution::

            from aerosol_optprop import Lognormal

            # create a lognormal distribution and compute the cross section
            aerosol = Lognormal(median_radius=0.1, width=1.5)
            aerosol.cross_section(wavelength=0.525)

            # update the particle number density using a fixed extinction
            aerosol.set_n_from_extinction(extinction=1e-3, wavelength=0.525, persistant=True)
            aerosol.n

            # calculate derived parameters
            aerosol.surface_area_density
            aerosol.mode_radius = 0.1  # set the mode radius of the distribution
            aerosol.median_radius  # median radius has been automatically updated
            aerosol.extinction(wavelength=0.525)  # extinction is unchanged
            aerosol.n  # number density has been updated

        """
        super().__init__(0)
        self.radius = None

        # distribution variables
        self._median_radius = median_radius
        self._width = width
        self._n = n

        # sampling at which to perform quadrature
        self.num_sigma = 7
        self.num_samples = 50

        # linearly or logarithmically space the quadrature points
        self.logspace = False

    @property
    def median_radius(self):
        return self._median_radius

    @median_radius.setter
    def median_radius(self, value):
        self._has_changed = True
        # maintain extinction if n is set through k
        if self._k is not None and self.persistent_extinction:
            old_xsec = self.cross_section(self._k_wavelength).extinction
            self._median_radius = value
            new_xsec = self.cross_section(self._k_wavelength).extinction
            self._n = self._n * old_xsec / new_xsec
        else:
            self._median_radius = value

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        self._has_changed = True
        # maintain extinction if n is set through k
        if self._k is not None and self.persistent_extinction:
            old_xsec = self.cross_section(self._k_wavelength).extinction
            self._width = value
            new_xsec = self.cross_section(self._k_wavelength).extinction
            self._n = self._n * old_xsec / new_xsec
        else:
            self._width = value

    @property
    def mode_radius(self):
        return self.median_radius * np.exp(-np.log(self.width)**2)

    @mode_radius.setter
    def mode_radius(self, value):
        self.median_radius = value * np.exp(np.log(self.width)**2)

    @property
    def surface_area_density(self):
        return 4 * np.pi * self._n * np.exp(2 * np.log(self.median_radius) + 2 * np.log(self.width) ** 2)

    @property
    def volume_density(self):
        return 4 / 3 * np.pi * self._n * np.exp(3 * np.log(self.median_radius) + 9 / 2 * np.log(self.width) ** 2)

    @property
    def radii(self):
        num = self.num_sigma * 2 * self.num_samples
        if self.logspace:
            return np.logspace((np.log(self.mode_radius) - self.num_sigma * np.log(self.width)) / np.log(10),
                               (np.log(self.mode_radius) + self.num_sigma * np.log(self.width)) / np.log(10), num)
        else:
            return np.linspace(np.exp(np.log(self.mode_radius) - self.num_sigma * np.log(self.width)),
                               np.exp(np.log(self.mode_radius) + self.num_sigma * np.log(self.width)), num)

    def pdf(self, radii: np.ndarray=None):
        """
        Return the lognormal distribution function

        Parameters
        ----------
        radii :
            array of radii in microns that the number density will be returned at. Defaults to the quadrature points
        """
        if radii is None:
            radii = self.radii

        return self._n / (np.sqrt(2 * np.pi) * radii * np.log(self.width)) * np.exp(
            -(np.log(radii / self.median_radius) ** 2) / (2 * np.log(self.width) ** 2))

    def cross_section(self, wavelength: float) -> CrossSection:
        """
        Calcualate the scattering, extinction and absorption cross sections of the per particle aerosol distribution

        Parameters
        ----------
        wavelength :
            wavelength in microns to compute the cross sections at

        Returns
        -------
        CrossSection
            named tuple containing 'scattering' 'extinction' and 'absorption' values
        """
        r = self.radii
        x = 2 * np.pi * r / wavelength
        n = self.pdf(r) / self._n

        qqsca = np.zeros(len(x))
        qqext = np.zeros(len(x))
        m = self.refractive_index(wavelength)

        for i in range(len(x)):
            qext, qsca, qback, g = mie.mie(m, x[i])
            qqsca[i] = qsca * np.pi * r[i] ** 2
            qqext[i] = qext * np.pi * r[i] ** 2

        return CrossSection(scattering=np.trapz(qqsca * n, r) / np.trapz(n, r) * (1e-4 ** 2),
                            extinction=np.trapz(qqext * n, r) / np.trapz(n, r) * (1e-4 ** 2),
                            absorption=np.trapz((qqext - qqsca) * n, r) / np.trapz(n, r) * (1e-4 ** 2))

    def asymmetry_factor(self, wavelength: float) -> float:
        """
        Calculate the assymetry factor, defined as

        .. math::
            g = \int_{4\pi} P(\Theta)\cos(\Theta)d\Omega

        Parameters
        ----------
        wavelength :
            wavelength in microns

        Returns
        -------
        float
            assymetry_factor
        """
        r = self.radii
        x = 2 * np.pi * r / wavelength
        n = self.pdf(r) / self._n

        asymm = np.zeros(len(x))
        qqsca = np.zeros(len(x))
        m = self.refractive_index(wavelength)

        for i in range(len(x)):
            _, qsca, _, asymm[i] = mie.mie(m, x[i])
            qqsca[i] = qsca * np.pi * r[i] ** 2

        return np.trapz(asymm * n * qqsca, r) / np.trapz(n * qqsca, r)

    def phase_function(self, wavelength: float, scattering_angles: np.ndarray=np.arange(0, 181)) -> np.ndarray:
        """
        Return the scattering phase function, normalized such that

        .. math::
            \int_{4\pi} P(\Theta)d\Omega = 4\pi

        Parameters
        ----------
        wavelength :
            wavelength in microns
        scattering_angles :
            angles in degrees at which to compute the phase function.
            Default is 0..180 in 1 degree steps

        Returns
        -------
        np.ndarray
            phase_function
        """
        r = self.radii
        n = self.pdf(r) / self._n
        x = 2 * np.pi * r / wavelength
        mu = np.cos(scattering_angles * np.pi / 180)
        p = np.zeros((len(x), len(scattering_angles)))
        qqsca = np.zeros(len(x))

        m = self.refractive_index(wavelength)
        for idx, (xi, ri) in enumerate(zip(x, r)):
            _, qqsca[idx], _, _ = mie.mie(m, xi)
            qqsca[idx] *= np.pi * ri**2
            if xi < 0.1:
                if np.real(m) == 0:
                    s1, s2 = mie.small_mie_conducting_S1_S2(m, xi, mu)
                else:
                    s1, s2 = mie.small_mie_S1_S2(m, xi, mu)
            else:
                s1, s2 = mie.mie_S1_S2(m, xi, mu)

            p[idx] = (np.abs(s1) ** 2 + np.abs(s2) ** 2) * 2 * np.pi

        return np.trapz(p * (n * qqsca)[:, np.newaxis], r, axis=0) / np.trapz(n * qqsca, r)
