from collections import namedtuple

CrossSection = namedtuple('CrossSection', ['scattering', 'extinction', 'absorption'])